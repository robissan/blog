const ENGINE = (function(){
  function u(x){
    if(x === null || typeof x === "undefined") return true
    return false
  }
  
  function hasClass(element, cls){ 
    return element.classList.contains(cls)
  }
  
  function classArray(element){
    return Array.from(element.classList)
  }
  
  function buildGfx(element, transformData){
    const ctx = element.getContext("2d")
    const width = element.width
    const height = element.height

    const xMin = transformData[0]
    const xMax = transformData[1]
    const yMin = transformData[2]
    const yMax = transformData[3]
    const xAbs = xMax - xMin
    const yAbs = yMax - yMin

    function setColour(c){
      if(u(c)) return
      ctx.fillStyle = c
    }
    function setColourStroke(c){
      if(u(c)) return
      ctx.strokeStyle = c
    }
    function clear(){
      ctx.clearRect(0, 0, width, height)
    }
    function square(x,y,s,c){
      setColour(c)
      ctx.fillRect(x,y,s,s)
    }
    function rect(x,y,w,h,c){
      setColour(c)
      ctx.fillRect(x,y,w,h)
    }
    function line(x1,y1,x2,y2,s,c){
      setColourStroke(c)
      ctx.beginPath()
      ctx.moveTo(x1,y1)
      ctx.lineTo(x2,y2)
      ctx.stroke()
    }
    function transform(x,y){
      const relx = (x - xMin) / xAbs
      const rely = (y - yMin) / yAbs
      // transform x,y in world coordinates to pixel height and width
      return {x: relx * width, y: height * (1 - rely)}
    }
    return {
      width: width,
      height: height,
      ctx: ctx,
      clear: clear,
      square: square,
      rect: rect,
      line: line,
      transform: transform
    }
  }
  
  function findTransformMeta(metaArray){
    // One meta should define transform data to use for transforming coordinates
    const transformMeta = metaArray.map(m => m.init()).find(data => !u(data.transform))
    if(u(transformMeta)){
      console.log("No metadata defined transform data.")
      return [0,1,0,1]
    }
    return transformMeta.transform
  }

  function attachEvents(element, metaArray, memoryArray){
    // Each event will need a pointer to memory
    metaArray.forEach((m,i) => {
      if(!u(m.event)){
        Object.keys(m.event).forEach(k => {
          // const event = m.event[k]
          element[k] = m.event[k](memoryArray[i])
        })
      }
    })
  }

  function buildEngine(meta, element){
    const classes = classArray(element)
    const metaArray = Object.keys(meta)
    .filter(cls => hasClass(element, cls))
    .sort((s1,s2) => classes.indexOf(s1) - classes.indexOf(s2))
    .map(k => meta[k])
    const memoryArray = metaArray.map(m => m.init())

    const gfx = buildGfx(element, findTransformMeta(metaArray))
  
    attachEvents(element, metaArray, memoryArray)

    return {
      start: function(){ 
        let lastTimeStamp =  performance.now()
  
        window.requestAnimationFrame(step)
  
        function step(timeStamp){
          window.requestAnimationFrame(step)
  
          const fps = 1000/(timeStamp - lastTimeStamp)
          const fpsS = fps.toString().slice(0,4)
          lastTimeStamp = timeStamp
  
          metaArray.forEach((m,i) => m.run(memoryArray[i], fpsS))
          metaArray.forEach((m,i) => m.render(gfx, memoryArray[i]))
        }
      }
    }
  }
  
  function buildEngines(meta){
  
    meta.fps = fpsEngineMeta()
    meta.clear = clearEngineMeta()
  
    return Array
      .from(document.getElementsByClassName("engine"))
      .map(element => buildEngine(meta, element))
  }
  
  // Standard Engine Meta

  // Display an FPS counter
  function fpsEngineMeta(){
    return {
      init: () => {
        return {
          fps: 0
        }
      },
      run: function(memory, fpsS){
        memory.fps = fpsS
      },
      render: function(gfx, memory){
        gfx.rect(5,0,25,15,"rgb(255,255,255)")
        gfx.ctx.fillStyle = "rgb(0,0,0)"
        gfx.ctx.strokeStyle = "rgb(0,0,0)"
        gfx.ctx.fillText(memory.fps, 10, 10)
      }
    }
  }

  // Clear the canvas
  function clearEngineMeta(){
    return {
      init: () => { return {} },
      run: function(memory, fpsS){},
      render: function(gfx, memory){
        gfx.clear()
      }
    }
  }

  return {
    buildEngines: buildEngines
  }
})()