// If module is defined, assume we are running in node.js (for tests)
const IS_ENGINE_NODE = typeof (module) !== "undefined"
const DEBUG = true

const TWOPI = 2 * Math.PI
const ROOT2 = Math.sqrt(2)

function u(x) {
  if (x === null || typeof x === "undefined") return true
  return false
}

function d(x) { return !u(x) }

const ENGINE = (function () {

  function zip(a, b, f) {
    if (u(a)) throw "Cannot zip undefined [a]"
    if (u(b)) throw "Cannot zip undefined [b]"
    if (u(f)) throw "Cannot zip undefined [f]"
    if (!Array.isArray(a)) throw "Cannot zip, a is not an array"
    if (!Array.isArray(b)) throw "Cannot zip, b is not an array"
    return a.map((itemA, indexA) => f(itemA, b[indexA]))
  }

  function getTransformFunctions(canvasWidth, canvasHeight, xMin, xMax, yMin, yMax) {
    function position(xy) {
      const relx = (xy[0] - xMin) / (xMax - xMin)
      const rely = 1 - (xy[1] - yMin) / (yMax - yMin)
      return [relx * canvasWidth, rely * canvasHeight]
    }
    function distance(dxdy) {
      // Distances are measured from 0, but our world mins may not be zero.
      // We do not need to flip y here. Or do we...?
      const relx = dxdy[0] / (xMax - xMin)
      const rely = dxdy[1] / (yMax - yMin)
      return [relx * canvasWidth, rely * canvasHeight]
    }
    function inversePosition(xyPx) {
      const relx = xyPx[0] / canvasWidth
      const rely = xyPx[1] / canvasHeight
      return [xMin + relx * (xMax - xMin), yMin + (1 - rely) * (yMax - yMin)]
    }
    function inverseDistance(xyPx) {
      const relx = xyPx[0] / canvasWidth
      const rely = xyPx[1] / canvasHeight
      return [relx * (xMax - xMin), rely * (yMax - yMin)]
    }
    return {
      position: position,
      distance: distance,
      inversePosition: inversePosition,
      inverseDistance: inverseDistance
    }
  }

  function hasClass(element, cls) {
    return element.classList.contains(cls)
  }

  function classArray(element) {
    return Array.from(element.classList)
  }

  function buildGfx(element, transformData) {

    // If a meta does not provide transform data, transform will be identity
    if (u(transformData)) {
      return buildGfxInner(element, 0, element.width, 0, element.height)
    }

    const xMin = transformData[0]
    const xMax = transformData[1]
    const yMin = transformData[2]
    const yMax = transformData[3]
    return buildGfxInner(element, xMin, xMax, yMin, yMax)
  }

  function buildGfxInner(element, xMin, xMax, yMin, yMax) {
    const ctx = element.getContext("2d")
    const width = element.width
    const height = element.height

    function setColour(c) {
      if (u(c)) return
      ctx.fillStyle = c
      ctx.strokeStyle = c
    }
    function clear() {
      ctx.clearRect(0, 0, width, height)
    }

    // Transformed functions, take parameters in world coordinates.
    // They should all take [x,y] pair arrays
    const theTransformFunctions = getTransformFunctions(element.width, element.height, xMin, xMax, yMin, yMax)
    const positionTransform = theTransformFunctions.position
    const distanceTransform = theTransformFunctions.distance
    const transformed = {
      // Direct access to transform functions
      f: {
        position: {
          world2px: positionTransform,
          px2world: theTransformFunctions.inversePosition
        },
        distance: {
          world2px: distanceTransform,
          px2world: theTransformFunctions.inverseDistance
        }
      },
      // Square, with sides equal length in world coordinates
      square: function (xy, s, c) {
        if (d(c)) { setColour(c) }
        // We need the top left in pixels...
        const xyPx = positionTransform(xy)
        const ssPx = distanceTransform([s, s])
        // BUT we want xyPx to be the centre point
        const centrePx = [xyPx[0] - ssPx[0] / 2, xyPx[1] - ssPx[1] / 2]
        ctx.fillRect(centrePx[0], centrePx[1], ssPx[0], ssPx[1])
      },
      rect: function (xy, ss, c) {
        if (d(c)) { setColour(c) }
        // We need the top left in pixels...
        const xyPx = positionTransform(xy)
        const ssPx = distanceTransform(ss)
        // BUT we want xyPx to be the centre point
        const centrePx = [xyPx[0] - ssPx[0] / 2, xyPx[1] - ssPx[1] / 2]
        ctx.fillRect(centrePx[0], centrePx[1], ssPx[0], ssPx[1])
      },
      rectWithGradient: function (xy, ss, fromColour, toColour) {
        const xyPx = positionTransform(xy)
        const ssPx = distanceTransform(ss)
        const centrePx = [xyPx[0] - ssPx[0] / 2, xyPx[1] - ssPx[1] / 2]
        const gradient = ctx.createLinearGradient(
          centrePx[0],
          centrePx[1],
          centrePx[0],
          centrePx[1] + ssPx[1]);
        gradient.addColorStop(0, fromColour);
        gradient.addColorStop(1, toColour);
        ctx.fillStyle = gradient
        ctx.fillRect(centrePx[0], centrePx[1], ssPx[0], ssPx[1])
      },
      // Square with sides equal length in pixels
      squareAbs: function (xy, sPx, c) {
        if (d(c)) { setColour(c) }
        const xyPx = positionTransform(xy)
        const centrePx = [xyPx[0] - sPx / 2, xyPx[1] - sPx / 2]
        ctx.fillRect(centrePx[0], centrePx[1], sPx, sPx)
      },
      line: function (xyStart, xyEnd, c, lineWidthPx) {
        if (d(c)) { setColour(c) }
        if (d(lineWidthPx)) { ctx.lineWidth = lineWidthPx }
        const xyStartPx = positionTransform(xyStart)
        const xyEndPx = positionTransform(xyEnd)
        ctx.beginPath()
        ctx.moveTo(xyStartPx[0], xyStartPx[1])
        ctx.lineTo(xyEndPx[0], xyEndPx[1])
        ctx.stroke()
      },
      path: function (xys, c, lineWidthPx) {
        if (d(c)) { setColour(c) }
        if (d(lineWidthPx)) { ctx.lineWidth = lineWidthPx }
        const xyPxs = xys.map(positionTransform)
        ctx.beginPath()
        ctx.moveTo(xyPxs[0][0], xyPxs[0][1])
        xyPxs.forEach(xy => ctx.lineTo(xy[0], xy[1]))
        ctx.stroke()
        ctx.fill()
      },
      ellipse: function (xy, xyRadii, rotation, c, lineWidthPx) {
        if (c) setColour(c)
        if (lineWidthPx) ctx.lineWidth = lineWidthPx
        const xyPx = positionTransform(xy)
        const radiiPx = distanceTransform(xyRadii)
        ctx.beginPath();
        ctx.ellipse(xyPx[0], xyPx[1], radiiPx[0], radiiPx[1], rotation, 0, TWOPI);
        ctx.fill();
      },
      stroke: {
        rect: function (xy, ss, c) {
          if (c) setColour(c)
          // We need the top left in pixels...
          const xyPx = positionTransform(xy)
          const ssPx = distanceTransform(ss)
          // BUT we want xyPx to be the centre point
          // This naming is dumb and wrong, will fix it...
          const centrePx = [xyPx[0] - ssPx[0] / 2, xyPx[1] - ssPx[1] / 2]
          ctx.strokeRect(centrePx[0], centrePx[1], ssPx[0], ssPx[1])
        },
        path: function (xys, c, lineWidthPx) {
          if (c) setColour(c)
          if (lineWidthPx) ctx.lineWidth = lineWidthPx
          const xyPxs = xys.map(positionTransform)
          ctx.beginPath()
          ctx.moveTo(xyPxs[0][0], xyPxs[0][1])
          xyPxs.forEach(xy => ctx.lineTo(xy[0], xy[1]))
          ctx.stroke()
        },
        ellipse: function (xy, xyRadii, rotation, c, lineWidthPx) {
          if (c) setColour(c)
          if (lineWidthPx) ctx.lineWidth = lineWidthPx
          const xyPx = positionTransform(xy)
          const radiiPx = distanceTransform(xyRadii)
          ctx.beginPath();
          ctx.ellipse(xyPx[0], xyPx[1], radiiPx[0], radiiPx[1], rotation, 0, TWOPI);
          ctx.stroke();
        }
      }
    }

    return {
      width: width,
      height: height,
      ctx: ctx,
      setColour: setColour,
      clear: clear,
      transformed: transformed
    }
  }

  function attachEvents(element, metaArray, gfxArray, memoryArray) {
    // Each event will need a pointer to memory
    metaArray.forEach((m, i) => {
      if (d(m.event)) {
        Object.keys(m.event).forEach(k => element[k] = m.event[k](gfxArray[i], memoryArray[i]))
      }
    })
  }

  function buildEngine(meta, element) {
    const classes = classArray(element)
    const metaArray = Object.keys(meta)
      .filter(cls => hasClass(element, cls))
      .sort((s1, s2) => classes.indexOf(s1) - classes.indexOf(s2))
      .map(k => meta[k])

    if (DEBUG) {
      console.log(`Found element [${element.id}]`)
      console.log(`It has meta: [${Object.keys(meta)}]`)
    }

    const memoryArray = metaArray.map(m => m.init())

    const gfxArray = memoryArray.map(m => {
      // Does this meta define a transform to use for graphics?
      if (d(m.transform)) {
        return buildGfx(element, m.transform)
      } else {
        return buildGfx(element, null)
      }
    })

    attachEvents(element, metaArray, gfxArray, memoryArray)

    // Run every runOnce
    metaArray.forEach((meta, i) => {
      if (d(meta.runOnce)) {
        meta.runOnce(gfxArray[i], memoryArray[i])
      }
    })

    return {
      start: function () {
        let lastTimeStamp = performance.now()

        window.requestAnimationFrame(step)

        function step(timeStamp) {
          window.requestAnimationFrame(step)

          const fps = 1000 / (timeStamp - lastTimeStamp)
          const fpsS = fps.toString().slice(0, 4)
          lastTimeStamp = timeStamp

          metaArray.forEach((m, i) => m.run(memoryArray[i], fpsS))
          metaArray.forEach((m, i) => m.render(gfxArray[i], memoryArray[i]))
        }
      }
    }
  }

  function buildEngines(meta) {
    meta.fps = fpsEngineMeta()
    meta.clear = clearEngineMeta()

    return Array
      .from(document.getElementsByClassName("engine"))
      .map(element => buildEngine(meta, element))
  }

  // Standard Engine Meta
  const RED = "rgb(170,32,43)"
  const GREEN = "rgb(24,295,43)"
  const BLACK = "rgb(0,0,0)"

  // Display an FPS counter
  function fpsEngineMeta() {
    return {
      init: () => {
        return {
          fps: 0,
          nFrame: 0,
          drawEvery: 60
        }
      },
      run: function (memory, fpsS) {
        if (memory.nFrame === 0) { memory.fps = fpsS }
        memory.nFrame = (memory.nFrame + 1) % memory.drawEvery
      },
      render: function (gfx, memory) {
        gfx.setColour(memory.fps > 50 ? GREEN : RED)
        gfx.ctx.fillRect(0, 0, 40, 20)
        gfx.setColour(BLACK)
        gfx.ctx.fillText(memory.fps, 10, 10)
      }
    }
  }

  // Clear the canvas
  function clearEngineMeta() {
    return {
      init: () => { return {} },
      run: function (memory, fpsS) { },
      render: function (gfx, memory) {
        gfx.clear()
      }
    }
  }

  // Allow access for tests
  if (IS_ENGINE_NODE) {
    return {
      zip: zip,
      getTransformFunctions: getTransformFunctions
    }
  }

  // Browser only has access to buildEngines
  return {
    buildEngines: buildEngines
  }
})()

if (IS_ENGINE_NODE) {
  module.exports = {
    functions: ENGINE,
    u: u,
    d: d,
  }
}