// Uses my updated rendering engine (engine2.js)

function dbg(x) {
  if (DEBUG) console.log(x)
}

function clip(clipTo, x) {
  return Math.max(-clipTo, Math.min(clipTo, x))
}

const A_FREQ = 440
const COLOUR_INPUT = "rgb(60,150,200)"
const COLOUR_OUTPUT = "rgb(200,60,150)"

// Setup sliders
// Global
const MASTER_VOLUME_SLIDER_ID = "masterVolumeSlider"
// These sliders write to sliderVals where they are accessed by engine run functions
const CROSSFADE_PURE_CLIPPED_ID = "crossfadePureClippedSlider"
const CLIPPING_AMOUNT_0_ID = "clippingAmountSlider0"
const CLIPPING_AMOUNT_1_ID = "clippingAmountSlider1"
const TRANSFER_FUNCTION_INPUT_SPAN_ID = "transferFunctionInput"
const TRANSFER_FUNCTION_OUTPUT_SPAN_ID = "transferFunctionOutput"
const CUSTOM_TRANSFER_FUNCTION = "custom-transfer-function"

const sliderVals = {}
Array.from(document.getElementsByClassName("slider"))
  .forEach(function (element) {
    if (element.id === MASTER_VOLUME_SLIDER_ID) return;
    sliderVals[element.id] = element.value
    element.oninput = function () {
      sliderVals[this.id] = this.value
    }
  })

// Master volume is not associated with an engine or canvas, so attach an event for it directly
document.getElementById(MASTER_VOLUME_SLIDER_ID).oninput = function () {
  dbg(`Master volume [${this.value}]`)
  AUDIO.setMasterVolume(this.value / 100)
}

// Transfer function buttons
const TF_BUTTON_CLIP_ID = "transfer-function-option-clip"
const TF_BUTTON_CLBS_ID = "transfer-function-option-clbs"
const TF_BUTTON_EXPO_ID = "transfer-function-option-expo"
const TF_BUTTON_QUAD_ID = "transfer-function-option-quad"
const TF_BUTTON_NOIS_ID = "transfer-function-option-nois"
const TF_BUTTON_NOI2_ID = "transfer-function-option-noi2"
const TF_BUTTON_LINE_ID = "transfer-function-option-line"

const TF_BUTTON_FOLD_ID = "transfer-function-option-fold"
const TF_BUTTON_WRAP_ID = "transfer-function-option-wrap"

let RANDOM_NUMBERS = generate(1000, () => Math.random())

const TRANSFER_FUNCTIONS = {}

// Hello! These are the definitions of the example transfer funtions.
// Each takes a value between -1 and 1 and returns a value on the same interval.
TRANSFER_FUNCTIONS[TF_BUTTON_CLIP_ID] = x => clip(0.5, x)
TRANSFER_FUNCTIONS[TF_BUTTON_CLBS_ID] = x => 2 * clip(0.5, x)
TRANSFER_FUNCTIONS[TF_BUTTON_EXPO_ID] = x => Math.tanh(Math.PI * x)
TRANSFER_FUNCTIONS[TF_BUTTON_QUAD_ID] = x => x * x
TRANSFER_FUNCTIONS[TF_BUTTON_NOIS_ID] = x => clip(1, x + RANDOM_NUMBERS[Math.floor(100 * (x + 1) / 2)] - 0.5)
TRANSFER_FUNCTIONS[TF_BUTTON_NOI2_ID] = x => (2 * RANDOM_NUMBERS[100 + Math.floor(100 * (x + 1) / 2)] - 1) * 0.5
TRANSFER_FUNCTIONS[TF_BUTTON_LINE_ID] = x => x

TRANSFER_FUNCTIONS[TF_BUTTON_FOLD_ID] = x => {
  const t = 0.5
  if (x > 0.5) return 1 - x
  if (x < -0.5) return -1 - x
  return x
}

TRANSFER_FUNCTIONS[TF_BUTTON_WRAP_ID] = x => {
  const t = 0.5
  if (x > 0.5) return x - 0.5
  if (x < -0.5) return x + 0.5
  return x
}

// Add all the "-copy" buttons to TRANSFER_FUNCTIONS
Object.keys(TRANSFER_FUNCTIONS).forEach(id => {
  const copyId = `${id}-copy`
  TRANSFER_FUNCTIONS[copyId] = TRANSFER_FUNCTIONS[id]
})


// Have the noisy buttons regenerate noise as well, just for fun
Object.keys(TRANSFER_FUNCTIONS).forEach(id => document.getElementById(id).onclick = () => {
  if (
    id == TF_BUTTON_NOIS_ID
    || id == TF_BUTTON_NOI2_ID
    || id == `${TF_BUTTON_NOIS_ID}-copy`
    || id == `${TF_BUTTON_NOI2_ID}-copy`) {
    RANDOM_NUMBERS = generate(1000, () => Math.random())
  }
  clickedTransferFunctionButton = id
})

// Save the last clicked here:
let clickedTransferFunctionButton = TF_BUTTON_CLIP_ID
// save the transfer function curve here
let tfCurve = generatef32(100, i => 2 * i / (100 - 1) - 1)

function applyTransferFunctionFromCurve(x, curve) {
  const n = curve.length
  // To get the output, we apply the transfer function to the input.
  // The curve defines the transfer function, we should linearly interpolate between curve values.
  // First find the index of the curve that applies to our input
  const index = (n - 1) * (clip(1, x) + 1) / 2
  // Interpolate between the lower and upper index (unless they are the same)
  const lowerIndex = Math.floor(index)
  const upperIndex = Math.ceil(index)
  let output;
  if (lowerIndex === upperIndex) {
    // Use exact value
    output = curve[lowerIndex]
  } else {
    // Interpolate
    // Ratio of index between lower and upper index
    const r = (index - lowerIndex) / (upperIndex - lowerIndex)
    const lowerOutput = curve[lowerIndex]
    const upperOutput = curve[upperIndex]
    output = r * upperOutput + (1 - r) * lowerOutput
  }
  return output
}

const AUDIO = (function () {
  // Safari Shim
  const AudioContext = window.AudioContext || window.webkitAudioContext;
  const ctx = new AudioContext()

  // Root gain node, to stop things being too loud even if master volume is all the way up
  const rootGainNode = ctx.createGain()
  rootGainNode.gain.setValueAtTime(0.01, ctx.currentTime)
  rootGainNode.connect(ctx.destination)

  masterVolumeNode = ctx.createGain()
  masterVolumeNode.gain.setValueAtTime(0.5, ctx.currentTime)
  masterVolumeNode.connect(rootGainNode)

  // Audio source nodes
  // Use the global tfCurve
  const audioDistortionNode = ctx.createWaveShaper()
  audioDistortionNode.curve = tfCurve
  audioDistortionNode.connect(masterVolumeNode)
  // Boost the signal going into the distortion because these are very quiet
  const audioBooster = ctx.createGain()
  audioBooster.gain.setValueAtTime(10, ctx.currentTime)
  audioBooster.connect(audioDistortionNode)
  // Actual source nodes
  const audioSourceNode0 = ctx.createMediaElementSource(document.getElementById("audio0"))
  const audioSourceNode1 = ctx.createMediaElementSource(document.getElementById("audio1"))
  audioSourceNode0.connect(audioBooster)
  audioSourceNode1.connect(audioBooster)

  const sounds = {}

  function getNeutralDistortionCurve() {
    const curve = new Float32Array(101)
    for (let i = 0; i < curve.length; i++) {
      curve[i] = 2 * i / (curve.length - 1) - 1
    }
    return curve
  }

  function getClippedDistortionCurve(clipTo) {
    const curve = new Float32Array(101)
    // Absolute value does not exceed clip
    for (let i = 0; i < curve.length; i++) {
      curve[i] = clip(clipTo, 2 * i / (curve.length - 1) - 1)
    }
    return curve
  }

  return {
    getClippedDistortionCurve: getClippedDistortionCurve,
    setMasterVolume: function (v) {
      masterVolumeNode.gain.setValueAtTime(v, ctx.currentTime)
    },
    setLocalVolume: function (id, v) {
      if (!sounds[id]) {
        return
      }
      sounds[id].localVolumeNode.gain.setValueAtTime(v, ctx.currentTime)
    },
    setDistortionCurve: function (id, curve) {
      if (!sounds[id]) {
        return
      }
      sounds[id].distortionNode.curve = curve
    },
    playNote: function (id, freqs, optionalDistortionCurve) {
      if (!id) { throw `ID [${id}] is not valid` }

      // Local volume to control only this sound
      const localVolumeNode = ctx.createGain()
      localVolumeNode.gain.setValueAtTime(1, ctx.currentTime)

      // Distortion will use a neutral curve (no effect) if none is provided
      const distortionNode = ctx.createWaveShaper()
      const curve = optionalDistortionCurve ? optionalDistortionCurve : getNeutralDistortionCurve()
      distortionNode.curve = curve

      const oscsWithFreqs = freqs
        .map(x => ({ freq: x, osc: ctx.createOscillator() }))

      oscsWithFreqs.forEach(t => t.osc.type = "sine")
      oscsWithFreqs.forEach(t => t.osc.frequency.setValueAtTime(t.freq, ctx.currentTime))

      // osc -> distortion -> local volume -> master volume
      localVolumeNode.connect(masterVolumeNode)
      distortionNode.connect(localVolumeNode)
      oscsWithFreqs.forEach(t => t.osc.connect(distortionNode))

      oscsWithFreqs.forEach(t => t.osc.start())

      sounds[id] = {
        oscsWithFreqs: oscsWithFreqs,
        localVolumeNode: localVolumeNode,
        distortionNode: distortionNode
      }
    },
    stopNote: function (id) {
      if (!sounds[id]) {
        dbg(`No sound with ID [${id}]`)
        return
      }
      sounds[id].oscsWithFreqs.forEach(t => t.osc.stop())
      sounds[id] = null
    },
    stopAll: function () {
      Object.keys(sounds).forEach(idToStop => this.stopNote(idToStop))
    },
    updateAudioCurve: function () {
      // Update curve of our audio distortion nodes
      audioDistortionNode.curve = tfCurve
    }
  }
})()

function generate(n, f) {
  const arr = new Array(n)
  for (let i = 0; i < arr.length; i++) {
    arr[i] = f(i)
  }
  return arr
}

function generatef32(n, f) {
  const arr = new Float32Array(n)
  for (let i = 0; i < arr.length; i++) {
    arr[i] = f(i)
  }
  return arr
}

function toUntyped(f32array) {
  const arr = new Array(f32array.length)
  for (let i = 0; i < f32array.length; i++) {
    arr[i] = f32array[i];
  }
  return arr
}

function greyscale(v) {
  return `rgb(${v},${v},${v})`
}

function combineWaveBuffer(freqs, n, phase, amp) {
  const fundamental = freqs[0]

  const waves = freqs.map(freq => {
    const phaseMultiply = freq / fundamental
    return generate(n, i => amp * Math.sin(TWOPI * freq * i / n + phase * phaseMultiply))
  })

  return waves
    .reduce((totalWave, currentWave) => totalWave.map((x, i) => x + currentWave[i]))
    .map(x => x / waves.length)
}

function combineWaveBufferWithForLoop(freqs, n, phase, amp) {
  const fundamental = freqs[0]
  const nFreqs = freqs.length

  // Do this old fashioned in a for loop, wonder if it's a bit of a performance killer using reduce/map
  // Can't actually see any difference though, sticking with neat version
  const totalWave = new Array(n)

  // Start with all zeros
  for (let i = 0; i < n; i++) {
    totalWave[i] = 0
  }

  for (let freqIndex = 0; freqIndex < nFreqs; freqIndex++) {
    const freq = freqs[freqIndex];
    const phaseMultiply = freq / fundamental
    // Add all the waves up
    for (let i = 0; i < n; i++) {
      totalWave[i] = totalWave[i] + amp * Math.sin(TWOPI * freq * i / n + phase * phaseMultiply)
    }
  }

  // Divide by number of waves
  for (let i = 0; i < n; i++) {
    totalWave[i] = totalWave[i] / nFreqs
  }

  return totalWave
}

const distortionMeta = {
  "clipped-sine": {
    init: () => {
      const xMin = 0
      const xMax = 2 * Math.PI
      const yMin = -1
      const yMax = 1
      const n = 500
      const phase = 0
      const freq = 3
      const amp = 0.95
      const clip = 0.5
      return {
        transform: [xMin, xMax, yMin, yMax],
        wave: {
          n: n,
          phase: phase,
          freq: freq,
          amp: amp,
          clip: clip
        },
        playing: false,
        cleanId: "clipped-sine-clean",
        clippedId: "clipped-sine-clipped",
        cleanBuffer: generate(n, i => amp * Math.sin(TWOPI * freq * i / n + phase)),
        clippedBuffer: generate(n, i => Math.min(clip, amp * Math.sin(TWOPI * freq * i / n + phase))),
      }
    },
    run: function (memory) {
      // Update any values based on sliders
      const crossfade = sliderVals[CROSSFADE_PURE_CLIPPED_ID] / 100
      memory.crossfade = crossfade
      const clipAmount = 1 - sliderVals[CLIPPING_AMOUNT_0_ID] / 100
      memory.wave.clip = clipAmount
      if (memory.playing) {
        memory.wave.phase = memory.wave.phase + 0.1
        // cross fade between pure and clipped
        AUDIO.setLocalVolume(memory.cleanId, 1 - crossfade)
        AUDIO.setLocalVolume(memory.clippedId, crossfade)
        AUDIO.setDistortionCurve(memory.clippedId, AUDIO.getClippedDistortionCurve(memory.wave.clip))
      }
      memory.cleanBuffer = generate(memory.wave.n, i => memory.wave.amp * Math.sin(TWOPI * memory.wave.freq * i / memory.wave.n + memory.wave.phase))
      memory.clippedBuffer = generate(memory.wave.n, i => clip(memory.wave.clip, memory.wave.amp * Math.sin(TWOPI * memory.wave.freq * i / memory.wave.n + memory.wave.phase)))
    },
    render: function (gfx, memory) {
      const xysClipped = memory.clippedBuffer.map((y, i) => [i * TWOPI / memory.wave.n, y])
      const xysClean = memory.cleanBuffer.map((y, i) => [i * TWOPI / memory.wave.n, y])
      gfx.transformed.stroke.path(xysClean, greyscale(200 * memory.crossfade), 2)
      gfx.transformed.stroke.path(xysClipped, greyscale(200 * (1 - memory.crossfade)), 5)
    },
    event: {
      onmouseup: function (gfx, memory) {
        return (mouse) => {
          memory.playing = !memory.playing
          dbg(`Playing [${memory.playing}]`)
          if (memory.playing) {
            AUDIO.playNote(memory.cleanId, [A_FREQ])
            AUDIO.playNote(memory.clippedId, [A_FREQ], AUDIO.getClippedDistortionCurve(memory.wave.clip))
          } else {
            AUDIO.stopNote(memory.cleanId)
            AUDIO.stopNote(memory.clippedId)
          }

        }
      }
    }
  },
  "clipped-multi-sine": {
    init: () => {
      const xMin = 0
      const xMax = 2 * Math.PI
      const yMin = -1
      const yMax = 1
      const n = 250
      const phase = 0
      const freq = 2
      const freqMultipliers = [1, 1.01, 0.99, 2, 2.01, 3, 3.01]
      const amp = 0.95
      const clip = 0.5
      return {
        transform: [xMin, xMax, yMin, yMax],
        wave: {
          n: n,
          phase: phase,
          freq: freq,
          amp: amp,
          clip: clip,
          freqMultipliers: freqMultipliers
        },
        playing: false,
        multiWaveId: "clipped-multi-sine-wave",
        buffer: combineWaveBuffer(freqMultipliers.map(f => f * freq), n, phase, amp)
      }
    },
    run: function (memory) {
      // Set the clip amount from slider
      const clipAmount = 1.001 - sliderVals[CLIPPING_AMOUNT_1_ID] / 100
      memory.wave.clip = clipAmount
      // Calculate amplitude modifier based on clip amount.
      // Clip of 0.5 should double amplitude.
      // This is a post-distortion volume change.
      const ampMod = 1 / memory.wave.clip

      if (memory.playing) {
        // Update phase (moves graph along)
        memory.wave.phase = memory.wave.phase + 0.2
        // Update distortion curve of the sound playing
        AUDIO.setDistortionCurve(memory.multiWaveId, AUDIO.getClippedDistortionCurve(memory.wave.clip))
        // And update the volume
        AUDIO.setLocalVolume(memory.multiWaveId, ampMod)
      }
      // Update the buffer which will get rendered
      const freqs = memory.wave.freqMultipliers.map(f => f * memory.wave.freq)
      memory.buffer = combineWaveBuffer(freqs, memory.wave.n, memory.wave.phase, memory.wave.amp)
        .map(x => clip(memory.wave.clip, x))
        .map(x => x * ampMod * memory.wave.amp)
    },
    render: function (gfx, memory) {
      const xys = memory.buffer.map((y, i) => [i * TWOPI / memory.wave.n, y])
      gfx.transformed.stroke.path(xys, greyscale(150), 2)
    },
    event: {
      onmouseup: function (gfx, memory) {
        return (mouse) => {
          memory.playing = !memory.playing
          dbg(`Playing [${memory.playing}]`)
          if (memory.playing) {
            const freqs = memory.wave.freqMultipliers.map(f => f * A_FREQ)
            AUDIO.playNote(memory.multiWaveId, freqs, AUDIO.getClippedDistortionCurve(memory.wave.clip))
          } else {
            AUDIO.stopNote(memory.multiWaveId)
          }
        }
      }
    }
  },
  "transfer-function": {
    init: () => {
      const xMin = -1
      const xMax = 1
      const yMin = -1
      const yMax = 1
      const n = 100
      const curve = generatef32(n, i => 2 * i / (n - 1) - 1)
      return {
        transform: [xMin, xMax, yMin, yMax],
        curve: curve,
        n: n,
        xyMouse: [0, 0],
        input: 0,
        output: 0
      }
    },
    run: function (memory) {
      // Recalculate curve
      // Generate values from -1 to 1 and map them using a transfer function
      memory.curve = generatef32(memory.n, i => 2 * i / (memory.n - 1) - 1)
        .map(TRANSFER_FUNCTIONS[clickedTransferFunctionButton])

      // Save the curve to an outside variable, we will reuse it in tf-sine
      tfCurve = memory.curve
      AUDIO.updateAudioCurve()

      // Get input and output values from xyMouse
      memory.input = clip(1, memory.xyMouse[0])
      memory.output = applyTransferFunctionFromCurve(memory.input, memory.curve)
      // Set the values in the spans
      document.getElementById(TRANSFER_FUNCTION_INPUT_SPAN_ID).innerHTML = memory.input.toFixed(2)
      document.getElementById(TRANSFER_FUNCTION_OUTPUT_SPAN_ID).innerHTML = memory.output.toFixed(2)
    },
    render: function (gfx, memory) {
      gfx.transformed.line([-1, 0], [1, 0], "rgb(100,100,100)", 1)
      gfx.transformed.line([0, -1], [0, 1], "rgb(100,100,100)", 1)
      const xys = toUntyped(memory.curve).map((y, i) => {
        const x = 2 * i / (memory.n - 1) - 1
        return [x, y]
      })
      gfx.transformed.stroke.path(xys, "rgb(100,100,100)", 2)
      // Input line
      gfx.transformed.line([memory.input, 0], [memory.input, memory.output], COLOUR_INPUT)
      // Output line
      gfx.transformed.line([0, memory.output], [memory.input, memory.output], COLOUR_OUTPUT)

    },
    event: {
      onmousemove: function (gfx, memory) {
        return (mouse) => {
          const xyPx = [mouse.offsetX, mouse.offsetY]
          memory.xyMouse = gfx.transformed.f.position.px2world(xyPx)
        }
      }
    }
  },
  "tf-sine": {
    init: () => {
      const xMin = 0
      const xMax = 2 * Math.PI
      const yMin = -1
      const yMax = 1
      const n = 500
      const phase = 0
      const freq = 3
      const amp = 0.95
      const cleanBuffer = generate(n, i => amp * Math.sin(TWOPI * freq * i / n + phase))
      return {
        transform: [xMin, xMax, yMin, yMax],
        wave: {
          n: n,
          phase: phase,
          freq: freq,
          amp: amp,
        },
        playing: false,
        clippedId: "tf-sine-clipped",
        cleanBuffer: cleanBuffer,
        functionAppliedBuffer: cleanBuffer.map(x => applyTransferFunctionFromCurve(x, tfCurve)),
      }
    },
    run: function (memory) {
      // Update any values based on sliders
      const crossfade = sliderVals[CROSSFADE_PURE_CLIPPED_ID] / 100
      memory.crossfade = crossfade
      const clipAmount = sliderVals[CLIPPING_AMOUNT_0_ID] / 100
      memory.wave.clip = clipAmount
      if (memory.playing) {
        memory.wave.phase = memory.wave.phase + 0.1
        // cross fade between pure and clipped
        AUDIO.setLocalVolume(memory.clippedId, crossfade)
        AUDIO.setDistortionCurve(memory.clippedId, tfCurve)
      }
      memory.cleanBuffer = generate(memory.wave.n, i => memory.wave.amp * Math.sin(TWOPI * memory.wave.freq * i / memory.wave.n + memory.wave.phase))
      memory.functionAppliedBuffer = memory.cleanBuffer.map(x => applyTransferFunctionFromCurve(x, tfCurve))
    },
    render: function (gfx, memory) {
      const xysClipped = memory.functionAppliedBuffer.map((y, i) => [i * TWOPI / memory.wave.n, y])
      const xysClean = memory.cleanBuffer.map((y, i) => [i * TWOPI / memory.wave.n, y])
      gfx.transformed.stroke.path(xysClean, COLOUR_INPUT, 2)
      gfx.transformed.stroke.path(xysClipped, COLOUR_OUTPUT, 2)
    },
    event: {
      onmouseup: function (gfx, memory) {
        return (mouse) => {
          memory.playing = !memory.playing
          dbg(`Playing [${memory.playing}]`)
          if (memory.playing) {
            AUDIO.playNote(memory.clippedId, [A_FREQ], tfCurve)
          } else {
            AUDIO.stopNote(memory.clippedId)
          }

        }
      }
    }
  },
}

ENGINE.buildEngines(distortionMeta).forEach(e => e.start())
