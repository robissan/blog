const WORLD_TOP = 100
const GEN_ID = (function(){
  var _id = -1
  return function(){
    _id++
    return _id
  }
})()

function newTree(x,dna){

  const state = {
    id: GEN_ID(),
    x: x,
    age: -1,
    energy: 0,
    height: 1,
    leafWidth: 0,
    trait: {
      maxHeight: 33,
      leafAge: 20*60,
      maxLeafWidth: 10,
      trunkColour: "rgb(165,42,42)",
      leafColour: "rgb(58, 125, 11)",
      spawnAge: 0*60,
      spawnChance: 0.01,
      theta: 0.5
    }
  }

  function renderTrunk(gfx){
    const topLeftPx = gfx.transform(x, state.height)
    gfx.rect(topLeftPx.x - 1,topLeftPx.y,2,topLeftPx.y,state.trait.trunkColour)
  }

  function renderLeaf(gfx){
    const leafLeft = x - (state.leafWidth / 2)
    const leafRight = x + (state.leafWidth / 2)
    const topLeftPx = gfx.transform(leafLeft, state.height)
    const topRightPx = gfx.transform(leafRight, state.height)
    const leafWidthpx = topRightPx.x - topLeftPx.x
    gfx.rect(topLeftPx.x, topLeftPx.y, leafWidthpx, 2, state.trait.leafColour)
  }

  function renderTree(gfx, memory){
    renderTrunk(gfx)
    renderLeaf(gfx)
  }

  function grow(){
    state.age++
    state.height += 0.01
    if(state.height > state.trait.maxHeight){ state.height = state.trait.maxHeight }
    if(state.age > state.trait.leafAge){
      state.leafWidth += 0.01
      if(state.leafWidth > state.trait.maxLeafWidth){ state.leafWidth = state.trait.maxLeafWidth }
    }
  }

  function left(treeState){
    return treeState.x - (treeState.leafWidth / 2)
  }

  function right(treeState){
    return treeState.x + (treeState.leafWidth / 2)
  }

  // True if any part of trees 1 and 2 intersect
  function intersects(treeState1, treeState2){
    if(left(treeState1)  > right(treeState2)) return false
    if(right(treeState1) < left(treeState2))  return false
    return true
  }

  function absorb(memory){
    // Set of trees T
    // Filter out any lower
    // And filter out any non intersecting

    const T_above = memory.life
      .filter(t => state.height < t.state.height)

      console.log(`Tree [${state.id}] has [${T_above.length}] above`)

    const T = T_above
      .filter(t => intersects(state, t.state))
    
    // Get the light profile at this height
    console.log(`Tree [${state.id}] has [${T.length}] intersections`)

    state.energy += 0.1
  }

  function trySpawn(memory){
    if(state.age > state.trait.spawnAge){
      if(Math.random() < state.trait.spawnChance){
        if(memory.life.length < 10){
          memory.life.push(spawn())
        }
      }
    }
  }

  function spawn(){
    return newTree(x + (Math.random() - 0.5) * 10, dna)
  }

  function runTree(memory){
    absorb(memory)
    grow()
    trySpawn(memory)
  }

  return {
    state: state,
    render: renderTree,
    run: runTree
  }
}

const meta = {
  trees: {
    init: () => {
      // Initial values to copy to memory
      const xMin = 0
      const xMax = 100
      const yMin = 0
      const yMax = WORLD_TOP
      return {
        transform: [xMin, xMax, yMin, yMax],
        life: [newTree(50,"")]
      }
    },
    run: function(memory){
      memory.life.forEach(l => l.run(memory))
    },
    render: function(gfx, memory){
      memory.life.forEach(l => l.render(gfx, memory))
    },
    event: {
      onmousedown: function(memory){ return () => console.log("onmousedown") },
      onmouseup: function(memory){ return () => console.log("onmouseup") }
    }
  }
}

ENGINE.buildEngines(meta).forEach(e => e.start())