﻿// Learn more about F# at http://fsharp.org

open System

let addOne x = x + 1
let square x = x * x
let double x = x * 2
let allThree = addOne << square << double

// Equivalent to
let squareThenAddOne_ x = addOne (square (double x))

[<EntryPoint>]
let main argv =
    printfn "allThree [%i]" (allThree 10)
    0 // return an integer exit code
