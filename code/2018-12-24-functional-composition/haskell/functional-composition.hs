addOne = (+) 1
square x = x * x
double = (*) 2
allThree = addOne . square . double

-- Equivalent to
allThree_ x = addOne (square (double x))

main :: IO ()
main = putStrLn $ show $ allThree 10