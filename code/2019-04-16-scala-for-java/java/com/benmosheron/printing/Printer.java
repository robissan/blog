package com.benmosheron.printing;

public class Printer{
  String pad = "";
  public Printer(String pad){
    this.pad = pad;
  }

  public void println(String s){
    System.out.println(pad + s);
  }
}