package com.benmosheron.scalaforjava;

import com.benmosheron.printing.Printer;

class SimpleJavaApp {
  public static void main(String[] args){
    Printer printer = new Printer("");
    Printer bulletPointPrinter =  new Printer("  - ");
    printer.println("Wow:");
    bulletPointPrinter.println("This is pretty great");
    bulletPointPrinter.println("Love the semicolons");
  }
}