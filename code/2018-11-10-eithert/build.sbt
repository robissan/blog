name := "EitherT Sample App"

description := "A fully functioning, self contained sample app. Run via sbt run."

version := "0.1"

scalaVersion := "2.12.5"

// cats needs this
scalacOptions += "-Ypartial-unification"

libraryDependencies += "org.typelevel" %% "cats-core" % "1.4.0"