---
layout: page
title: About
permalink: /about/
---

My name is Ben Sheron, and this is my blog. I don't have anything to put here yet.

If I make this public, you'll be able to see all the source code [here](https://gitlab.com/benmosheron/blog/tree/master/code).
